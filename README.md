# L'IGN fait disparaître petit à petit les adresses FTP, suivant leur dire : Le ftp étant de moins en moins supporté par les navigateurs et ne répondant plus à une diffusion des données en open data pour tous les publics, nous avons basculer les données en HTTP. Le fichier joint devient donc petit à petit inutile


# Fichier compatible avec Filezilla
Retrouver la liste des FTP IGN sur leur site (Attention, il existe des liens au format HTTPS:// qui ne peuvent pas être répertoriés dans filezilla).

- [site de l'IGN](https://geoservices.ign.fr/documentation/diffusion/telechargement-donnees-libres.html)
- [article du blog pasq.fr](https://pasq.fr/les-ftp-donnees-libres-ign)

Pour télécharger, clic_droit sur le fichier et "enregistrer le lien sous..." ou "enregistrer la cible du lien sous..."

Dans filezilla --> fichier --> importer les paramètres
